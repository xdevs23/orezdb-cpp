# orezdb

Database uhhhh

# How to build

```bash
./generate.sh
make all
```

The resulting file is in `bin/main`.

# License

All source files and the resulting binary in this repository are licensed under the AGPLv3 as outlined below:

```
    orezdb
    Copyright (C) 2021  Simão Gomes Viana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
