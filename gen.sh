#!/usr/bin/env bash

set -euo pipefail

case $1 in
  objpaths)
    while read -r preobj; do
      echo "obj/$(echo "$preobj" | sed -re 's/^src\/(.*)$/\1/g')"
    done < <(./find.sh sources | sed -re 's/[.]c(pp)?$/.o/g')
    ;;
esac
