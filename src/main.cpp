#include <iostream>
#include <cstdlib>
#include <csignal>
#include <thread>

#include "db/Database.h"
#include "log.h"

using namespace orezdb;
using namespace orezdb::db;

using std::thread;

const char* test = "test";

extern void sigsegv_handler(int sig);
extern void sigabrt_handler(int sig);
extern void generic_stop_handler(int sig);

const int test_count = 1*100*1000;
const int big_test_count = 1000;
const int huge_test_count = 100;

static void testSTM() {
    LOG(" -> Testing STM");
    auto map = stm::SearchTreeMap<uint64_t>{};
    for (uint64_t i = 0; i < test_count; i++) {
        for (uint64_t j = 0; j < 5; j++) {
            map.put(i, j);
            if (i % 10 == j) {
                if (map[i] != j) {
                    LOGD();
                    LOGEF("\nWrong result %lu != %lu\n", map[i], j);
                    throw std::logic_error("error in implementation: operator [] returned wrong or unrelated result");
                }
            }
        }
        if (i % 100 == 0) {
            map.remove(i);
        }
        if (i % (200*1000) == 0) {
            LOGD_STDERR("\n   * checking map size...\n");
            if (map.size() > 400 * 1000) {
                LOGDF_STDERR("   ! map very big (%zu, detected at %lu), clearing.\n", map.size(), i);
                map.clear();
                LOGDF_STDERR("   -> map was cleared, new size: %zu (should be 0)\n", map.size());
            }
        }
        if (i % 1000 == 0) {
            CLEAR_LINE_STDERR();
            LOGDF_STDERR("size: %zu", map.size());
        }
    }
}

static void testAllocation(Database* database) {
    LOG(" -> Testing allocation functionality");

    auto alloc = database->allocate(100);
    alloc->overwrite((char) 0xFE);

    auto alloc2 = database->allocate(100);
    alloc2->overwrite((char) 0xAB);

    alloc = database->getAllocation(alloc->addr());
    alloc->write(0x42, 10, 6);

    delete alloc2;
    alloc->discard();
    delete alloc;

    LOGF("Testing %d iterations of small allocations\n", test_count);
    for (int i = 0; i < test_count; i++) {
        fprintf(stderr, "\r%d %lu ", i, pthread_self());
        alloc = database->allocate(rand() % 100 + i % (rand() % 10 + 2) + 2);
        size_t size = rand() % alloc->allocationSize();
        size = std::min<size_t>(std::max<size_t>(size, 10), alloc->allocationSize());
        alloc->overwrite((char) (rand() % 0xFF));
        auto offs = (rand() % 10) % size;
        alloc->write(rand() % 0xFF, std::min(size - offs, size), offs);
        delete alloc;
    }
    printf("\n");

    LOGF("Testing %d iterations of big allocations\n", big_test_count);
    for (int i = 0; i < big_test_count; i++) {
        fprintf(stderr, "\r%d %lu ", i, pthread_self());
        alloc = database->allocate(rand() % 10000 + i*2 + 10);
        size_t size = rand() % alloc->allocationSize();
        size =  std::min<size_t>(std::max<size_t>(size, 100), alloc->allocationSize());
        alloc->overwrite((char) (rand() % 0xFF));
        auto offs = (rand() % 10) % size;
        alloc->write(rand() % 0xFF, std::min(size - offs, size), offs);
        delete alloc;
    }
    printf("\n");

    LOGF("Testing %d iterations of huge allocations (without writing data into it)\n", big_test_count);
    for (int i = 0; i < huge_test_count; i++) {
        fprintf(stderr, "\r%d %lu ", i, pthread_self());
        alloc = database->allocate(rand() % 20*1000*1000 + 100*1000);
        fprintf(stderr, " size: %lu", alloc->allocationSize());
        delete alloc;
    }
    printf("\n");

    LOG("Deleting all allocations");
    database->clearAllocations();
}

Database* database;

void testAll() {
    testAllocation(database);
}

int main() {
    signal(SIGSEGV, sigsegv_handler);
    signal(SIGABRT, sigabrt_handler);
#ifdef HEAP_DEBUG
    signal(SIGINT, generic_stop_handler);
    signal(SIGSTOP, generic_stop_handler);
#endif

    //testSTM();
    //testReadWrite(database);

    database = new Database();
    database->from("test/db");

    thread thread1 = thread(testAll);
    thread thread2 = thread(testAll);
    thread thread3 = thread(testAll);
    thread thread4 = thread(testAll);
    thread thread5 = thread(testAll);
    thread thread6 = thread(testAll);
    thread thread7 = thread(testAll);
    thread thread8 = thread(testAll);

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    thread5.join();
    thread6.join();
    thread7.join();
    thread8.join();

    LOG("Everything completed successfully.");

    return 0;
}