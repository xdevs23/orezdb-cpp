#pragma once

#include <cstdlib>

#define LOCK_GUARD std::lock_guard _(lock)

namespace orezdb::sugar {

    template <typename T>
    inline T if_null(T ptr, T alt) {
        if (ptr == nullptr) {
            return alt;
        }
        return ptr;
    }

    template <typename T>
    inline T zero_value_of() {
        auto empty = T{};
        return empty;
    }

}
