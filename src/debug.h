#pragma once

#ifdef DEBUG
#define DEBUG_ONLY(code) code
#else
#define DEBUG_ONLY
#endif