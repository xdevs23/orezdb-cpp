
#include <thread>

#include "Database.h"
#include "../threading/threads.h"

using std::thread;
using orezdb::threading::Future;
using orezdb::threading::Waiter;
using orezdb::threading::join_thread;
using orezdb::threading::set_thread_name;

#define WHILE_ACTIVE while (!shutdown.load())

namespace orezdb::db {

    Database::Database() {
        LOG("orezdb database");
        allocator = new DatabaseAllocator(this);

        // Spin off a consumer thread so that we can keep all operations synchronized
        consumer_thread = thread([this] {
            LOGD("Starting consumer thread");
            /**
             * Loops wait for events from the producer, so they
             * won't busy-wait
             */
            consumerThread();
        });
        set_thread_name(consumer_thread, "database request consumer");
    }

    DatabaseAllocation* Database::getAllocation(uint64_t offs) {
        auto req = new get_alloc_request{
                .offs = offs,
                .result = new Future<DatabaseAllocation*>(),
        };
        get_alloc_queue->provide(req);
        // This blocks until we have a result
        auto result = req->result->get();
        delete req->result;
        delete req;
        return result;
    }

    DatabaseAllocation* Database::allocate(uint64_t size) {
        auto req = new alloc_request{
            .size = size,
            .result = new Future<DatabaseAllocation*>(),
        };
        alloc_queue->provide(req);
        // This blocks until we have a result
        auto result = req->result->get();
        delete req->result;
        delete req;
        return result;
    }

    void Database::deallocate(DatabaseAllocation* alloc) {
        auto req = new dealloc_request{
            .alloc = alloc,
            .close_only = false,
            .waiter = new Waiter(),
        };
        dealloc_queue->provide(req);
        req->waiter->wait();
        delete req->waiter;
        delete req;
    }

    void Database::closeAllocation(DatabaseAllocation* alloc) {
        auto req = new dealloc_request{
            .alloc = alloc,
            .close_only = true,
            .waiter = new Waiter(),
        };
        dealloc_queue->provide(req);
        req->waiter->wait();
        delete req->waiter;
    }

    void Database::from(char* dir) {
        allocator->from(dir);
    }

    Database::~Database() {
        shutdown = true;
        join_thread(consumer_thread);
        delete allocator;
        delete alloc_queue;
        delete dealloc_queue;
        delete get_alloc_queue;
    }

    void Database::consumerThread() {
        LOGD("Started consumer thread.");
        thread alloc_requests_thread = thread([this] {
            LOGD("Starting alloc request consumer thread");
            consumeAllocRequestsEventLoop();
        });
        thread dealloc_requests_thread = thread([this] {
            LOGD("Starting dealloc request consumer thread");
            consumeDeallocRequestsEventLoop();
        });
        thread get_alloc_requests_thread = thread([this] {
            LOGD("Starting get alloc request consumer thread");
            consumeGetAllocRequestsEventLoop();
        });

        set_thread_name(alloc_requests_thread, "database alloc request consumer");
        set_thread_name(dealloc_requests_thread, "database dealloc request consumer");
        set_thread_name(get_alloc_requests_thread, "database get alloc request consumer");

        // Join all threads here so that we can wait for them to terminate
        join_thread(alloc_requests_thread);
        join_thread(dealloc_requests_thread);
        join_thread(get_alloc_requests_thread);
    }

    void Database::consumeAllocRequestsEventLoop() {
        LOGD("Started alloc request consumer thread");
        WHILE_ACTIVE {
            auto req = alloc_queue->consume();
            alloc_mutex.lock();
            auto result = allocator->allocate(req->size);
            alloc_mutex.unlock();
            req->result->set(result);
        }
    }

    void Database::consumeDeallocRequestsEventLoop() {
        LOGD("Started dealloc request consumer thread");
        WHILE_ACTIVE {
            auto req = dealloc_queue->consume();
            alloc_mutex.lock();
            if (req->close_only) {
                allocator->closeAllocation(req->alloc);
            } else {
                allocator->deallocate(req->alloc);
            }
            alloc_mutex.unlock();
            req->waiter->notify();
        }
    }

    void Database::consumeGetAllocRequestsEventLoop() {
        LOGD("Started get alloc request consumer thread");
        WHILE_ACTIVE {
            auto req = get_alloc_queue->consume();
            auto result = allocator->getAllocation(req->offs);
            req->result->set(result);
        }
    }

    void Database::clearAllocations() {
        allocator->clearAllocations();
    }

    void Database::write(void* src, uint64_t offs, uint64_t size) {
        allocator->write(src, offs, size);
    }


}