#pragma once

namespace orezdb::db {

    typedef struct {
        uint32_t page_count;
        uint32_t root_alloc_offs;
    } database_info;

    typedef struct database_page_t {
        uint32_t size;
        char* data;
        struct database_page_t* next;
        struct database_page_t* prev;
        int fd;
    } database_page;

    typedef union {
        bool has_next : 1;
        uint16_t flagsI;
    } alloc_flags;

    typedef struct database_alloc_t {
        uint64_t size;
        uint64_t offs;
        uint64_t next_offs;
        uint64_t prev_offs;
        alloc_flags flags;
        char reserved[32 - (
                                   sizeof(size) +
                                   sizeof(offs) +
                                   sizeof(next_offs) +
                                   sizeof(prev_offs) +
                                   sizeof(flags)
                           ) % 32];
    } database_alloc;

    typedef struct {
        uint64_t next_free_offs;
        uint64_t last_alloc_offs;
    } root_allocation;

}