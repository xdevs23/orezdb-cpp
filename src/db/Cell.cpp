#include "Cell.h"

#include <cstdint>
#include <cstring>

namespace orezdb::db {
    uint64_t Cell::size() {
        return byte_count;
    }

    void Cell::setData(void* src) {
        memcpy(data, src, byte_count);
    }

    void* Cell::getData() {
        return data;
    }
}
