#pragma once

#include <stdexcept>
#include <cstdlib>

// Forward declaration for DatabaseAllocator.h
namespace orezdb::db {
    class DatabaseAllocation;
}

#include "database_types.h"
#include "Database.h"
#include "DatabaseAllocator.h"
#include "../threading/ReadWriteLock.h"

using std::runtime_error;
using orezdb::threading::ReadWriteLock;

namespace orezdb::db {

    class DatabaseAllocation {
    private:
        Database* db = nullptr;
        database_alloc* alloc = nullptr;
        void* data = nullptr;
        threading::ReadWriteLock lock{};

        static runtime_error error_unusable();
        void checkUsability(int unlock = 0, const char* calledFrom = __builtin_FUNCTION());
        void overwrite_(char c);
        void erase_();
        void persistData();

        void writeHeader();
    protected:
    public:

        DatabaseAllocation(Database* db, database_alloc* alloc, void* data);
        ~DatabaseAllocation();

        uint64_t addr();
        uint64_t addr_unlocked();
        bool usable();
        void write(void* src, size_t size, size_t offs);
        void write(void* src, size_t size);
        void write(void* src);
        void write(char c, size_t size, size_t offs = 0);
        void erase();
        void overwrite(char c);
        void discard();
        DatabaseAllocation* prev();
        DatabaseAllocation* next();
        bool isLast();
        void setNext(DatabaseAllocation* next);
        void setPrev(DatabaseAllocation* prev);
        void persistMeta();

        // TODO: resize
        uint64_t allocationSize();
    };

}