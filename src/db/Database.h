#pragma once

#include <thread>
#include <atomic>

namespace orezdb::db {
    class Database;
}

#include "database_types.h"
#include "DatabaseAllocation.h"
#include "Database.h"
#include "DatabaseAllocator.h"
#include "../collections/SynchronizedQueue.h"
#include "../threading/Future.h"

using std::mutex;
using std::atomic;
using std::thread;

using orezdb::threading::Future;
using orezdb::threading::Waiter;
using orezdb::collections::SynchronizedQueue;

namespace orezdb::db {

    typedef struct {
        uint64_t size;
        Future<DatabaseAllocation*>* result;
    } alloc_request;

    typedef struct {
        DatabaseAllocation* alloc;
        bool close_only;
        Waiter* waiter;
    } dealloc_request;

    typedef struct {
        uint64_t offs;
        Future<DatabaseAllocation*>* result;
    } get_alloc_request;

    class Database {
    private:
        DatabaseAllocator* allocator;

        mutex alloc_mutex{};
        SynchronizedQueue<alloc_request*>* alloc_queue = new SynchronizedQueue<alloc_request*>();
        SynchronizedQueue<dealloc_request*>* dealloc_queue = new SynchronizedQueue<dealloc_request*>();
        SynchronizedQueue<get_alloc_request*>* get_alloc_queue = new SynchronizedQueue<get_alloc_request*>();

        atomic<bool> shutdown = false;
        thread consumer_thread;

        void consumerThread();
        void consumeAllocRequestsEventLoop();
        void consumeDeallocRequestsEventLoop();
        void consumeGetAllocRequestsEventLoop();

    public:
        Database();
        ~Database();
        DatabaseAllocation* getAllocation(uint64_t offs);
        DatabaseAllocation* allocate(uint64_t size);
        void deallocate(DatabaseAllocation* alloc);
        void from(char* dir);

        void clearAllocations();

        void write(void* src, uint64_t offs, uint64_t size);

        void closeAllocation(DatabaseAllocation* alloc);
    };
}

