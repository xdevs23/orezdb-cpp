#pragma once

#include <cstdint>

namespace orezdb::db {
    class Cell {
    private:
        uint64_t byte_count;
        void* data;

    public:
        uint64_t size();
        void setData(void* src);
        void* getData();
    };
}

