#include <sys/mman.h>
#include <unistd.h>
#include <cstdio>
#include <stdexcept>
#include <cstring>
#include <fcntl.h>
#include <libgen.h>

#include "DatabaseAllocator.h"
#include "DatabaseAllocation.h"
#include "../fs/dirs.h"
#include "../log.h"
#include "../fs/files.h"
#include "../safe_malloc.h"
#include "../collections/SearchTreeMap.h"

using namespace std;
using namespace orezdb;

namespace orezdb::db {

    const char* header = "orez\x42\x00\xfe\xff";
    const char* section_page_count = "pc";
    const char* section_root_alloc_offs = "rao";
    const size_t buf_size = 8192;

    void DatabaseAllocator::from(char* dir) {
        LOGF("DatabaseAllocator directory: %s\n", dir);
        setup(dir);

        createIfNotExists();
        readInfo();
        initialize();

        ready = true;
    }

    void DatabaseAllocator::createIfNotExists() {
        if (access(infoFilename, F_OK)) {
            createInfo();
            createRootAllocation();
        }
    }

    void DatabaseAllocator::createPage() {
        char pageFilename[maxLenPageFilename()];
        pageFileName(pageFilename, info.page_count++);

        FILE* file = fopen(pageFilename, "w");
        if (!file) {
            const char* error = "could not create file %s";
            int len = strlen(pageFilename);
            char buf[strlen(error) + len];
            sprintf(buf, error, pageFilename);
            throw runtime_error(buf);
        }


        char buf[buf_size];
        memset(buf, 0, buf_size);
        size_t to_write = DB_DEFAULT_PAGE_SIZE;
        do {
            fwrite(buf, min<size_t>(buf_size, to_write), 1, file);
            to_write -= buf_size;
        } while (to_write > 0);

        fflush(file);
        fclose(file);

        writeInfo();
    }

    void DatabaseAllocator::createInfo() {
        LOG("New database, creating info file");
        info.page_count = 0;
        fs::mkdirp(directory);
        writeInfo();
    }


    /**
     * name needs to be of len DB_PAGE_MAX_NAME_LEN
     * @param name out
     * @param no database_page number
     */
    void DatabaseAllocator::pageName(char* name, int no) {
        sprintf(name, "%s%d", DB_PAGE_NAME_PREFIX, no);
    }

    void DatabaseAllocator::setup(char* dir) {
        LOG("Setting up...");
        directory = mm::malloc<char>(strlen(dir) + 1);
        strncpy(directory, dir, strlen(dir));

        infoFilename = mm::malloc<char>(maxLenInfoFilename());
        infoFileName(infoFilename);

        pages = new list<database_page*>();
        LOGF("info file: %s\n", infoFilename);
    }

    DatabaseAllocator::~DatabaseAllocator() {
        LOG("DatabaseAllocator destructor called");
        if (lastAlloc) {
            mm::free(lastAlloc);
        }
        if (directory) {
            mm::free(directory);
        }
        if (infoFilename) {
            mm::free(infoFilename);
        }
        while (!pages->empty()) {
            database_page* page = pages->back();
            unmapPage(page);
        }
        sync();
    }

    void DatabaseAllocator::pageFileName(char* filename, int no) {
        char basename[DB_PAGE_MAX_NAME_LEN];
        pageName(basename, no);

        sprintf(filename, "%s/%s", directory, basename);
    }

    void DatabaseAllocator::infoFileName(char* filename) {
        sprintf(filename, "%s/%s", directory, DB_PAGE_INFO_BASENAME);
    }

    int DatabaseAllocator::maxLenPageFilename() {
        return (int) strlen(directory) + 1 + DB_PAGE_MAX_NAME_LEN + 1;
    }

    int DatabaseAllocator::maxLenInfoFilename() {
        return (int) strlen(directory) + 1 + DB_PAGE_INFO_BASENAME_LEN + 1;
    }

    void DatabaseAllocator::writeInfo() {
        LOGD("Writing info...");

        FILE* file = fopen(infoFilename, "w");
        fwrite(header, DB_HEADER_LEN, 1, file);
        fwrite(section_page_count, strlen(section_page_count), 1, file);
        fwrite(&info.page_count, sizeof(info.page_count), 1, file);

        fwrite(section_root_alloc_offs, strlen(section_root_alloc_offs), 1, file);
        fwrite(&info.root_alloc_offs, sizeof(info.root_alloc_offs), 1, file);

        fflush(file);
        fclose(file);

        LOGD("Info successfully written.\n");
    }

    void DatabaseAllocator::readInfo() {
        LOG("Reading database info...");
        FILE* file = fopen(infoFilename, "r");

        char read_header[DB_HEADER_LEN];
        fread(read_header, DB_HEADER_LEN, 1, file);
        if (*(uint64_t*) read_header != *(uint64_t*) header) {
            throw runtime_error("info header mismatch");
        }

        fseek(file, strlen(section_page_count), SEEK_CUR);
        fread(&info.page_count, sizeof(info.page_count), 1, file);

        fseek(file, strlen(section_root_alloc_offs), SEEK_CUR);
        fread(&info.root_alloc_offs, sizeof(info.root_alloc_offs), 1, file);

        fclose(file);


        LOG("DatabaseAllocator info read successfully.");
        LOGF("Page count: %d\n", info.page_count);
        LOG("\n");
    }

    void DatabaseAllocator::mapPage(int no) {
        LOGDF("Mapping page %d\n", no);

        if (no < pages->size()) {
            LOGD("Already mapped, skipping.");
        }
        char pageFilename[maxLenPageFilename()];
        pageFileName(pageFilename, no);

        auto size = fs::fileSize(pageFilename);

        LOGDF("  -> page file %s, size %lu\n", pageFilename, size);

        int fd = open(pageFilename, O_RDWR);
        if (!fd) {
            throw runtime_error("unexpected file open error");
        }

        void* region = mmap(
                nullptr,
                size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                fd,
                0
        );
        if (region == MAP_FAILED) {
            LOGF("could not mmap file %s\n", pageFilename);
            perror("Could not mmap");
            throw runtime_error("mmap of DB failed");
        }

        trackPage(page(size, region, fd));
    }

    void DatabaseAllocator::trackPage(database_page* page) {
        LOGDF("Tracking page with size %d and fd %d\n", page->size, page->fd);
        database_page* lastBeforePush = nullptr;
        if (!pages->empty()) {
            lastBeforePush = pages->back();
            lastBeforePush->next = page;
        }
        page->prev = lastBeforePush;
        pages->push_back(page);
        LOGDF("New mapped page count: %zu\n\n", pages->size());
    }

    void DatabaseAllocator::untrackPage(database_page* page) {
        LOGDF("Untracking page with size %d and fd %d\n", page->size, page->fd);
        if (page->prev) {
            page->prev->next = page->next;
        }
        if (page->next) {
            page->next->prev = page->prev;
        }
        pages->remove(page);
        mm::free(page);
        LOGDF("New mapped page count: %zu\n\n", pages->size());
    }

    database_page* DatabaseAllocator::page(uint32_t size, void* region, int fd) {
        auto* page = mm::zmalloc<database_page>();
        page->prev = nullptr;
        page->next = nullptr;
        page->size = size;
        page->data = (char*) region;
        page->fd = fd;
        return page;
    }

    static alloc_flags allocFlags(bool has_next = false) {
        alloc_flags flags{
                .has_next = false,
        };
        return flags;
    }

    void DatabaseAllocator::mapNextPage() {
        mapPage(pages->size());
    }

    void DatabaseAllocator::requestNextPage() {
        LOGD(" * next page requested");
        if (info.page_count == pages->size()) {
            LOGD(" ! at the end of pages, creating new one");
            createPage();
        }
        mapNextPage();
    }

    int DatabaseAllocator::read(void* ptr, size_t offs, size_t len) {
        size_t to_read = len;
        database_page* page = pageAt(offs);
        do {
            if (!page) {
                return 1;
            }
            uint32_t already_read = len - to_read;
            uint32_t relative_offs = pageToRelativeOffset(page, offs + already_read);
            uint32_t remaining_total_space =
                    pageRemainingTotalSpace(page, relative_offs);
            uint32_t len_in_page = min<uint32_t>(to_read, remaining_total_space);
            memcpy(&((char*) ptr)[already_read], &page->data[relative_offs], len_in_page);
            to_read -= len_in_page;
            page = page->next;
        } while (to_read > 0);
        return 0;
    }

    void DatabaseAllocator::write(void* data, size_t offs, size_t len) {
        size_t to_write = len;
        database_page* page = pageAt(offs, true);
        do {
            if (!page) {
                requestNextPage();
                page = pages->back();
            }
            uint32_t already_written = len - to_write;
            uint32_t relative_offs = pageToRelativeOffset(page, offs + already_written);

            uint32_t remaining_total_space =
                    pageRemainingTotalSpace(page, relative_offs);
            uint32_t len_in_page = min<uint32_t>(to_write, remaining_total_space);
#ifdef SUPER_VERBOSE
            LOGDF("To write: %zu, already written: %d, abs offs: %d, relative offs: %d, remaining: %d, len in page: %d\n",
                  to_write, already_written, offs, relative_offs, remaining_total_space, len_in_page);
#endif

            memcpy(&page->data[relative_offs], &((char*) data)[already_written], len_in_page);
            to_write -= len_in_page;
            page = page->next;
        } while (to_write > 0);
    }

    database_page* DatabaseAllocator::pageAt(size_t offs, bool request_if_absent) {
        uint64_t cur_offs = 0;
        if (pages->empty() && pages->size() < info.page_count) {
            mapNextPage();
        } else if (pages->empty() && request_if_absent) {
            requestNextPage();
        }
        database_page* page = pages->front();
        int page_no = 0;
        do {
            cur_offs += page->size;
            if (cur_offs >= offs) {
                return page;
            }
            page = page->next;
            if (!page) {
                if (info.page_count - 1 == page_no) {
                    if (request_if_absent) {
                        requestNextPage();
                        page = pages->back();
                    }
                } else {
                    mapNextPage();
                    page = pages->back();
                }
            }
            page_no++;
        } while (page);
        throw runtime_error("page not found");
    }

    int32_t DatabaseAllocator::pageRemainingTotalSpace(database_page* page, size_t offs_page_start) {
        return page->size - offs_page_start;
    }

    uint32_t DatabaseAllocator::pageOffset(database_page* of_page) {
        uint32_t cur_offs = 0;
        for (auto page : *pages) {
#ifdef SUPER_VERBOSE
            LOGDF("offs: %d\n", cur_offs);
#endif
            if (page == of_page) {
                return cur_offs;
            } else {
                cur_offs += page->size;
            }
        }
        throw runtime_error("page not found");
    }

    uint32_t DatabaseAllocator::pageToRelativeOffset(database_page* page, size_t offs) {
        return offs - pageOffset(page);
    }

    void DatabaseAllocator::unmapPage(database_page* page) {
        LOGDF("Unmapping page with size %d and fd %d\n", page->size, page->fd);
        munmap(page, page->size);
        close(page->fd);
        untrackPage(page);
    }

    DatabaseAllocator::DatabaseAllocator(Database* db) {
        this->db = db;
    }

    void DatabaseAllocator::createRootAllocation() {
        LOGD("Creating rootAlloc allocation...");

        root_allocation root{
                .next_free_offs = sizeof(database_alloc) + sizeof(root_allocation),
                .last_alloc_offs = 0,
        };

        database_alloc a{
                .size = sizeof(root),
                .offs = 0,
                .next_offs = 0,
                .prev_offs = 0,
                .flags = allocFlags()
        };

        write(&a, 0, sizeof(a));
        write(&root, a.offs + sizeof(database_alloc), a.size);

        info.root_alloc_offs = 0;

        writeInfo();
        LOGD("Root allocation created at offs 0");
    }

    void DatabaseAllocator::initialize() {
        LOG("Initializing...");
        rootAlloc = mm::malloc<root_allocation>();
        read(rootAlloc, info.root_alloc_offs + sizeof(database_alloc), sizeof(*rootAlloc));
        LOG("Initialized.");
        LOGDF("Next free offset: %lu, last offs: %lu\n", rootAlloc->next_free_offs, rootAlloc->last_alloc_offs);
    }

    static inline database_alloc* new_db_alloc() {
        return mm::zmalloc<database_alloc>();
    }

    DatabaseAllocation* DatabaseAllocator::createAllocation(size_t size) {
#ifdef DEBUG
        if (size > 200L*1024*1024) {
            LOGWF("Huge allocation! %zu", size);
            throw runtime_error("You're trying to allocate a lot, maybe it's a bug?");
        }
#endif
        auto new_alloc = new_db_alloc();
        new_alloc->size = size;
        new_alloc->offs = rootAlloc->next_free_offs;
        // Align to 8 bytes
        auto padding = DB_ALLOC_ALIGNMENT - new_alloc->offs % DB_ALLOC_ALIGNMENT;
        if (padding < DB_ALLOC_ALIGNMENT) {
            new_alloc->offs += padding;
        }

        auto alloc_data = mm::zmalloc<void>(size);
        auto last = lastAllocation();

        auto alloc = new DatabaseAllocation(db, new_alloc, alloc_data);
        alloc->setPrev(last);
        last->setNext(alloc);

        rootAlloc->next_free_offs = alloc->addr() + sizeof(database_alloc) + new_alloc->size;
        rootAlloc->last_alloc_offs = alloc->addr();

        write(rootAlloc, info.root_alloc_offs + sizeof(database_alloc), sizeof(root_allocation));

        alloc->persistMeta();
        last->persistMeta();

        alloc->erase();
        return alloc;
    }

    DatabaseAllocation* DatabaseAllocator::lastAllocation() {
        return getAllocation_(rootAlloc->last_alloc_offs);
    }

    void DatabaseAllocator::freeAllocation(DatabaseAllocation* alloc) {
        if (!alloc || !alloc->usable()) {
            // Looks like this one is already freed, don't do anything then.
            return;
        }

        DatabaseAllocation* prev;
        DatabaseAllocation* next;

        free_lock.lock();
        if (!alloc->usable()) {
            // Same thing here, but it seems like it happened off-thread
            free_lock.unlock();
            return;
        }

        prev = alloc->prev();
        if (!prev) {
            throw runtime_error("root allocation is not allowed to be removed");
        }
        next = alloc->next();

        prev->setNext(next);
        if (next) {
            next->setPrev(prev);
        }
        if (alloc->isLast()) {
            lastAlloc = prev;
            rootAlloc->last_alloc_offs = lastAlloc->addr();
        }

        prev->persistMeta();
        if (next) {
            next->persistMeta();
        }

        free_lock.unlock();
    }

    DatabaseAllocation* DatabaseAllocator::allocate(uint64_t size) {
        LOGDF("allocating size %lu\n", size);
        auto alloc = createAllocation(size);

        openAllocs.put(alloc->addr(), alloc);

        LOGDF("allocated at %lu\n", alloc->addr());

        return alloc;
    }

    void DatabaseAllocator::deallocate(DatabaseAllocation* alloc) {
        if (!alloc || !alloc->usable()) {
            return;
        }
        closeAllocation(alloc);
        freeAllocation(alloc);
    }

    DatabaseAllocation* DatabaseAllocator::getAllocation_(uint64_t offs) {
        auto alloc = openAllocs[offs];
        if (!alloc) {
            auto db_alloc = mm::malloc<database_alloc>();
            read(db_alloc, offs, sizeof(database_alloc));
            auto alloc_data = mm::malloc<void>(db_alloc->size);
            read(alloc_data, db_alloc->offs + sizeof(database_alloc), db_alloc->size);

            alloc = new DatabaseAllocation(db, db_alloc, alloc_data);
            openAllocs.put(offs, alloc);
        }
        return alloc;
    }

    void DatabaseAllocator::erase(size_t offs, size_t len) {
        char buf[len];
        memset(buf, 0, len);
        write(buf, offs, len);
    }

    void DatabaseAllocator::closeAllocation(DatabaseAllocation* alloc) {
        free_lock.lock();
        if (!alloc || !alloc->usable()) {
            // So this one is already freed
            free_lock.unlock();
            return;
        }
        openAllocs.remove(alloc->addr());
        free_lock.unlock();
    }

    void DatabaseAllocator::clearAllocations() {
        LOGDF("Clearing all allocations, last: %lu\n", rootAlloc->last_alloc_offs);
#ifdef DEBUG
        int i = 0;
#endif
        while (rootAlloc->last_alloc_offs != 0) {
            LOGDF("clearing %d\n", i++);
            auto last = lastAllocation();
            last->discard();
            delete last;
        }
    }

    DatabaseAllocation* DatabaseAllocator::getAllocation(uint64_t offs) {
        auto a = getAllocation_(offs);
        return a;
    }

}