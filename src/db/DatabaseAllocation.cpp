
#include <stdexcept>
#include <cstring>

#include "DatabaseAllocation.h"
#include "../log.h"
#include "../safe_malloc.h"

#define UNLOCK_READ 1
#define UNLOCK_WRITE 2

namespace orezdb::db {

    uint64_t DatabaseAllocation::addr() {
        checkUsability(UNLOCK_READ);
        auto offs = alloc->offs;
        return offs;
    }

    runtime_error DatabaseAllocation::error_unusable() {
        return runtime_error("this allocation is not usable!");
    }

    inline void DatabaseAllocation::checkUsability(int unlock, const char* calledFrom) {
        if (!usable()) {
            LOGEF("%s: unusable allocation (probably deallocated), called from %s", __func__, calledFrom);
            if (unlock == UNLOCK_READ) {
            } else if (unlock == UNLOCK_WRITE) {
            }
            throw error_unusable();
        }
    }

    bool DatabaseAllocation::usable() {
        return alloc != nullptr && data != nullptr && db != nullptr;
    }

    /**
     * Writes data specified at pointer src to the region determined by this allocation.
     * If the specified size is bigger than the accessible region indicated by src, expect undefined behavior.
     * Overwrites region excess of size with zeroes.
     *
     * @throws runtime_error if size specified exceeds region size of allocation
     * @param src source data
     * @param size size of source data or smaller; may not be bigger than allocation region size
     */
    void DatabaseAllocation::write(void* src, size_t size) {
        checkUsability(UNLOCK_WRITE);
        if (size > alloc->size) {
            LOGEF("specified size %zu is bigger than allocation size of %lu; allocation unmodified.\n",
                  size, alloc->size);
            throw runtime_error("allocation data write error");
        }
        if (size > 0) {
            memcpy(data, src, size);
        }
        if (size < alloc->size) {
            memset(&((char*) data)[size], 0, alloc->size - size);
        }
        persistData();
    }

    /**
     * Writes data specified at pointer src to the region determined by this allocation.
     * If the specified size is bigger than the accessible region indicated by src, expect undefined behavior.
     * No data is overwritten before offs and after offs + size.
     * This is pretty much just a memcpy.
     *
     * @throws runtime_error if size specified exceeds region size of allocation
     * @param src source data
     * @param size size of source data or smaller; may not be bigger than allocation region size
     * @param offs offset to start writing at. end = offs + size
     */
    void DatabaseAllocation::write(void* src, size_t size, size_t offs) {
        checkUsability(UNLOCK_WRITE);
        if (offs + size > alloc->size) {
            LOGEF("specified size(%zu)+offset(%zu) %zu is bigger than allocation size of %lu; allocation unmodified.\n",
                  size, offs, size + offs, alloc->size);
            throw runtime_error("allocation data write error");
        }
        if (size > 0 && offs >= 0) {
            memcpy(&((char*) data)[offs], src, size);
            persistData();
        }
    }

    /**
     * Writes data specified at pointer src to the region determined by this allocation.
     *
     * @param src source data; region must be at least size of allocation
     */
    void DatabaseAllocation::write(void* src) {
        write(src, alloc->size);
    }

    inline void DatabaseAllocation::erase_() {
        overwrite_(0);
    }

    void DatabaseAllocation::erase() {
        checkUsability(UNLOCK_WRITE);
        erase_();
    }

    inline void DatabaseAllocation::overwrite_(char c) {
        memset(data, c, alloc->size);
        persistData();
    }

    void DatabaseAllocation::overwrite(char c) {
        checkUsability(UNLOCK_WRITE);
        overwrite_(c);
    }

    void DatabaseAllocation::discard() {
        lock.lock_write();
        checkUsability(UNLOCK_WRITE);
        LOGDF("Discarding alloc at %lu, size %lu, prev %lu, next %lu\n",
              alloc->offs, alloc->size, alloc->prev_offs, alloc->next_offs);
        auto offs = alloc->offs;
        if (offs == 0) {
            throw runtime_error("this is the root allocation which cannot be discarded");
        }
        erase_();
        db->deallocate(this);
        memset(alloc, 0, sizeof(database_alloc));
        db->write(alloc, offs, sizeof(database_alloc));
        mm::free(data);
        data = nullptr;
        mm::free(alloc);
        alloc = nullptr;
        db = nullptr;
        lock.unlock_write();
    }

    DatabaseAllocation::DatabaseAllocation(Database* db, database_alloc* alloc, void* data) {
        this->db = db;
        this->alloc = alloc;
        this->data = data;
    }

    DatabaseAllocation* DatabaseAllocation::prev() {
        checkUsability(UNLOCK_READ);
        auto a = db->getAllocation(alloc->prev_offs);
        return a;
    }

    DatabaseAllocation* DatabaseAllocation::next() {
        checkUsability(UNLOCK_READ);
        DatabaseAllocation* n = nullptr;
        if (alloc->flags.has_next) {
            n = db->getAllocation(alloc->next_offs);
        }
        return n;
    }

    bool DatabaseAllocation::isLast() {
        checkUsability(UNLOCK_READ);
        return !alloc->flags.has_next;
    }

    void DatabaseAllocation::setNext(DatabaseAllocation* next) {
        checkUsability(UNLOCK_WRITE);
        alloc->next_offs = next ? next->addr() : 0;
        alloc->flags.has_next = next != nullptr;
    }

    void DatabaseAllocation::setPrev(DatabaseAllocation* prev) {
        checkUsability(UNLOCK_WRITE);
        alloc->prev_offs = prev ? prev->addr() : 0;
    }

    void DatabaseAllocation::persistMeta() {
        checkUsability();
        if (!usable()) {
            return;
        }
        writeHeader();
    }

    void DatabaseAllocation::writeHeader() {
        db->write(alloc, alloc->offs, sizeof(database_alloc));
    }

    DatabaseAllocation::~DatabaseAllocation() {
        if (usable()) {
            db->closeAllocation(this);

            if (data) {
                mm::free(data);
                data = nullptr;
            }
            if (alloc) {
                mm::free(alloc);
                alloc = nullptr;
            }
        }
    }

    void DatabaseAllocation::persistData() {
        LOGDF("persisting data at %lu, size %lu\n", alloc->offs, alloc->size);
        db->write(data, alloc->offs + sizeof(database_alloc), alloc->size);
    }

    /**
     * Works exactly like memset
     */
    void DatabaseAllocation::write(char c, size_t size, size_t offs) {
        checkUsability(UNLOCK_WRITE);
        if (size + offs > alloc->size) {
            LOGEF("specified size %zu + offs %zu is bigger than allocation size of %lu; allocation unmodified.",
                  size, offs, alloc->size);
            throw runtime_error("allocation data write error");
        }
        if (size > 0) {
            memset(&((char*) data)[offs], c, size);
            persistData();
        }
    }

    uint64_t DatabaseAllocation::allocationSize() {
        auto s = alloc->size;
        return s;
    }


}
