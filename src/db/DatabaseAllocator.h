#pragma once

#include <list>
#include <cstdint>
#include <mutex>

namespace orezdb::db {
    class DatabaseAllocator;
}

#include "database_types.h"
#include "DatabaseAllocation.h"
#include "Database.h"
#include "../collections/SearchTreeMap.h"
#include "../threading/ReadWriteLock.h"
#include "../threading/Lock.h"

#define DB_DEFAULT_PAGE_SIZE 64*1024*1024
#define DB_PAGE_PREFIX_LEN 4
#define UINT32_STR_LEN 10 // "4294967295"
#define DB_PAGE_MAX_NAME_LEN DB_PAGE_PREFIX_LEN + UINT32_STR_LEN + 1
#define DB_PAGE_NAME_PREFIX "page"
#define DB_PAGE_INFO_BASENAME "info"
#define DB_PAGE_INFO_BASENAME_LEN 4
#define DB_HEADER_LEN 8
#define DB_ALLOC_ALIGNMENT sizeof(uint64_t)

using std::list;
using std::mutex;
using namespace orezdb::collections;

namespace orezdb::db {

    extern const char* header;
    extern const char* section_page_count;
    extern const char* section_root_alloc_offs;

    class DatabaseAllocator {
        Database* db;

        database_info info{};
        list<database_page*>* pages{};
        bool ready = false;
        char* directory = nullptr;
        char* infoFilename = nullptr;
        root_allocation* rootAlloc = nullptr;
        DatabaseAllocation* lastAlloc = nullptr;
        stm::SearchTreeMap<DatabaseAllocation*> openAllocs{};
        threading::ReadWriteLock lock{};
        threading::Lock free_lock{};
        threading::ReadWriteLock page_lock{};
        mutex fs_lock{};
        mutex page_req_lock{};

        void setup(char* dir);
        void createIfNotExists();
        static void pageName(char* name, int no);
        int maxLenPageFilename();
        int maxLenInfoFilename();
        void pageFileName(char* filename, int no);
        void infoFileName(char* filename);

        void mapPage(int no);
        void unmapPage(database_page* page);
        void createPage();
        void trackPage(database_page* page);
        void untrackPage(database_page* page);
        static database_page* page(uint32_t size, void *data, int fd);
        void mapNextPage();
        void requestNextPage();
        database_page* pageAt(size_t offs, bool request_if_absent = false);
        uint32_t pageOffset(database_page* page);
        uint32_t pageToRelativeOffset(database_page* page, size_t offs);
        static int32_t pageRemainingTotalSpace(database_page* page, size_t offs);

        void writeInfo();
        void createInfo();
        void readInfo();

        void createRootAllocation();

        DatabaseAllocation* createAllocation(size_t size);
        void freeAllocation(DatabaseAllocation* alloc);
        DatabaseAllocation* lastAllocation();

        void erase(size_t offs, size_t len);

    protected:


    public:
        DatabaseAllocator(Database* db);
        ~DatabaseAllocator();
        void from(char* dir);

        // TODO: protect when tables are there
        int read(void* ptr, size_t offs, size_t len);
        void write(void* data, size_t offs, size_t len);

        DatabaseAllocation* getAllocation(uint64_t offs);
        /**
         * Do not use outside of db namespace
         */
        DatabaseAllocation* getAllocation_(uint64_t offs);
        DatabaseAllocation* allocate(uint64_t size);
        void deallocate(DatabaseAllocation* alloc);
        void closeAllocation(DatabaseAllocation* alloc);

        void initialize();

        void clearAllocations();
    };

}
