#pragma once

#include <cstdint>
#include <list>

#include "Row.h"

using std::list;

namespace orezdb::db {

    typedef uint64_t id;

    typedef struct {
        char* name;
        size_t db_offs;
    } table_info;

    class Table {
    private:
        table_info info;
        list<Row*> rows;
    public:
        id insert(Row& row);
        void update(Row& row);
        Row& get(id id);
        void remove(Row& row);
    };
}

