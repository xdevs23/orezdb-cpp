#include <climits>
#include <cstdio>
#include <cstring>
#include <sys/stat.h>
#include <libgen.h>

namespace orezdb::fs {
    void mkdirp(const char* dir) {
        char tmp[PATH_MAX];
        char *p = nullptr;
        size_t len;

        snprintf(tmp, sizeof(tmp), "%s", dir);
        len = strlen(tmp);
        if(tmp[len - 1] == '/')
            tmp[len - 1] = 0;
        for(p = tmp + 1; *p; p++)
            if(*p == '/') {
                *p = 0;
                mkdir(tmp, S_IRWXU);
                *p = '/';
            }
        mkdir(tmp, S_IRWXU);
    }

}

