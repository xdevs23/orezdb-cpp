#pragma once

#include <cstdlib>

namespace orezdb::fs {
    size_t fileSize(char* filename);
}
