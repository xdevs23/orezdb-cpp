
#include <cstdlib>
#include <sys/stat.h>

namespace orezdb::fs {
    size_t fileSize(char* filename) {
        struct stat st{};
        stat(filename, &st);
        return st.st_size;
    }
}
