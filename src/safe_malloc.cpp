
#ifdef DEBUG
#include <cstdlib>
#include <atomic>
#ifdef HEAP_DEBUG
#include <list>
#include "threading/Lock.h"

using namespace orezdb::threading;
#endif

using namespace std;
namespace orezdb::mm {
    atomic<size_t> malloc_count;
    atomic<size_t> free_count;
    atomic<size_t> alloc_count;
    atomic<size_t> malloc_size_ever;
#ifdef HEAP_DEBUG
    list<void*> malloc_pointers{};
    list<size_t> malloc_sizes{};
    threading::Lock heap_debug_lock{};
#endif
}
#endif

