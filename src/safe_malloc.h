#pragma once

#include <cstdlib>
#include <stdexcept>
#include <cstring>
#include <list>
#include "log.h"

#ifdef DEBUG
#include <atomic>
using std::atomic;
#endif

#ifdef HEAP_DEBUG
#include "threading/Lock.h"
using namespace orezdb::threading;
#endif

namespace orezdb::mm {

#ifdef DEBUG
    extern atomic<size_t> malloc_count;
    extern atomic<size_t> free_count;
    extern atomic<size_t> alloc_count;
    extern atomic<size_t> malloc_size_ever;
#ifdef HEAP_DEBUG
    extern std::list<void*> malloc_pointers;
    extern std::list<size_t> malloc_sizes;
    extern threading::Lock heap_debug_lock;
#endif
#endif

    template<typename T>
    inline T* malloc(size_t size = sizeof(T), bool zero = false) {
        void* ptr = ::malloc(size);
        if (!ptr) {
            throw std::runtime_error("cannot safely allocate memory (malloc failed). Out of memory?");
        }
        if (zero) {
            ::memset(ptr, 0, size);
        }
#ifdef DEBUG
        alloc_count++;
        malloc_count++;
        malloc_size_ever += size;
#ifdef HEAP_DEBUG
        malloc_sizes.push_back(size);
        malloc_pointers.push_back(ptr);
#endif
#endif
        return (T*) ptr;
    }

    template <typename T>
    inline T* zmalloc(size_t size = sizeof(T)) {
        return malloc<T>(size, true);
    }

    template<typename T>
    inline void free(
            T* ptr
#ifdef HEAP_DEBUG
            ,
            const char* calledFrom = __builtin_FUNCTION(),
            const char* callerFile = __builtin_FILE(),
            const int callerLine = __builtin_LINE()
#endif
    ) {
#ifdef DEBUG
        if (malloc_count <= free_count) {
            LOGWF("mm::free used more often (%zu) than mm::malloc (%zu), delta %zu, double free?\n",
                  free_count + 1, malloc_count.load(), free_count + 1 - malloc_count);
        }
#ifdef HEAP_DEBUG
        heap_debug_lock.lock();
        bool found = false;
        size_t index = 0;
        for (void* m_ptr : malloc_pointers) {
            if (m_ptr == ptr) {
                found = true;
                break;
            }
            index++;
        }
        if (!found) {
            LOGD_STDERR("");
            LOGDF_STDERR(
                    "\n%s: call from %s: double-free or invalid/unknown pointer to mm::free detected for %p\n",
                    __func__, calledFrom, ptr
            );
            LOGDF_STDERR(
                    "File: %s\nLine: %d\nFunction: %s\n\n",
                    callerFile, callerLine, calledFrom
            );
            heap_debug_lock.unlock();
            return;
        } else {
            auto mp_it = malloc_pointers.begin();
            std::advance(mp_it, index);
            malloc_pointers.erase(mp_it);
            auto ms_it = malloc_sizes.begin();
            std::advance(ms_it, index);
            malloc_sizes.erase(ms_it);
        }
        heap_debug_lock.unlock();
#endif
#endif
        ::free(ptr);
#ifdef DEBUG
        alloc_count--;
        free_count++;
#endif
    }

    template<typename T>
    inline T* toheap(T* ptr) {
        auto h_ptr = malloc<T>();
        memcpy(h_ptr, ptr, sizeof(T));
        return h_ptr;
    }
}
