#include <cstdio>
#include <execinfo.h>
#include <csignal>
#include <cstdlib>
#include <unistd.h>
#include <cstdio>
#include "safe_malloc.h"

using namespace orezdb;

void crash_handler() {
    fprintf(stderr, "\n");
    fprintf(stderr, "Flushing stdout...\n");
    printf("\n");
    auto stdout_ = stdout;
    stdout = fopen("/dev/null", "w");
    fflush(stdout_);
    fprintf(stderr, "stdout is muted now.\n");

    void *trace[16];
    size_t backtrace_size  = backtrace(trace, 16);
    backtrace_symbols_fd(trace, backtrace_size, STDERR_FILENO);
#ifdef DEBUG
    fprintf(stderr, "\n");
    fprintf(stderr, "Memory statistics for own code:\n");
    fprintf(stderr, "\tcurrent heap allocation count: %zu\n", mm::alloc_count.load());
    fprintf(stderr, "\tmalloc count: %zu\n", mm::malloc_count.load());
    fprintf(stderr, "\tfree count: %zu\n", mm::free_count.load());
    fprintf(stderr, "\tdelta: %ld\n", std::abs((long) (mm::free_count - mm::malloc_count)));
    fprintf(stderr, "\tmalloc size ever: %zu (%zu KiB, %zu MiB, %zu GiB)\n",
            mm::malloc_size_ever.load(),
            mm::malloc_size_ever.load() / 1024,
            mm::malloc_size_ever.load() / 1024 / 1024,
            mm::malloc_size_ever.load() / 1024 / 1024 / 1024);
#ifdef HEAP_DEBUG
    size_t total_mem = 0;
    size_t total_mallocs = 0;
    for (size_t size : mm::malloc_sizes) {
        total_mem += size;
        total_mallocs++;
    }
    fprintf(stderr, "\tcurrent heap memory usage: %zu (%zu KiB, %zu MiB, %zu GiB)\n",
            total_mem, total_mem / 1024, total_mem / 1024 / 1024, total_mem / 1024 / 1024 / 1024);
    fprintf(stderr, "\tcurrent malloc count: %zu\n", total_mallocs);
#endif
    fprintf(stderr, "\n");
#else
    fprintf(stderr, "Not printing memory statistics (only available in debug builds)\n");
#endif
    fflush(stderr);
}

void sigsegv_handler(int sig) {
    if (sig != SIGSEGV) {
        exit(sig);
    }
    fprintf(stderr, "\n");
    fprintf(stderr, "Fatal error: Segmentation fault (Signal %d):\n", sig);
    crash_handler();
    exit(sig);
}

void sigabrt_handler(int sig) {
    if (sig != SIGABRT) {
        exit(sig);
    }
    fprintf(stderr, "\n");
    fprintf(stderr, "Fatal error: Aborted (Signal %d):\n", sig);
    crash_handler();
    exit(sig);
}

void generic_stop_handler(int sig) {
    fprintf(stderr, "\n");
    fprintf(stderr, "STOP: Signal %d:\n", sig);
    crash_handler();
    exit(sig);
}
