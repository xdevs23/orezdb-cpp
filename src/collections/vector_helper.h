#pragma once

#include <vector>
#include <algorithm>
#include "../log.h"
#include "../debug.h"

namespace orezdb::collections::vector_helper {

    template<typename T>
    inline void remove(std::vector<T> &v, const T &item) {
        v.erase(std::remove(v.begin(), v.end(), item), v.end());
    }

}