#pragma once

#include <cstdlib>
#include <sys/types.h>
#include <list>
#include <vector>
#include <mutex>

#include "../safe_malloc.h"
#include "../sugar.h"
#include "../log.h"
#include "../debug.h"
#include "vector_helper.h"
#include "../threading/ReadWriteLock.h"

namespace orezdb::collections::stm {

#define NODE_LIST_REMOVE_L(list, elem) (list).remove((elem))
#define NODE_LIST_REMOVE_V(list, elem) vector_helper::remove(list, elem)
#define NODE_LIST_REMOVE(list, elem) NODE_LIST_REMOVE_V(list, elem)
#define NODE_LIST_RESERVE false

    template <typename T>
    struct node_t;

    template <typename T>
    using node_list_type = std::vector<struct node_t<T>*>;

    template <typename T>
    struct node_t {
        node_list_type<T>* children;
        struct node_t<T>* parent;
        uint8_t k;
        T v;
        bool has_value : 1;
    };

    template <typename T>
    using node = node_t<T>;

    template <typename T>
    struct key_t {
        T key;
        size_t size;
        bool deref;
    };

    template <typename T>
    using key = key_t<T>;

    template <typename T>
    key<T> as_key(T k) {
        return key<T>{
            .key = k,
            .size = sizeof(k),
        };
    }

    template <typename T>
    key<T> as_key_deref(T k, size_t size = sizeof(*k)) {
        return key<T>{
            .key = k,
            .size = size,
            .deref = true,
        };
    }


    template <typename T>
    static node_list_type<T>* new_node_list() {
        auto list = new node_list_type<T>();
#if NODE_LIST_RESERVE
        list->reserve(UINT8_MAX + 1);
#endif
        return list;
    }

    template <typename T>
    class SearchTreeMap {
    private:
        node_list_type<T>* children = new_node_list<T>();
        size_t value_count = 0;
        threading::ReadWriteLock lock = threading::ReadWriteLock("SearchTreeMap");

        template <typename K>
        node<T>* traverse(key<K> key);

        void remove_node(node<T>* node);

    public:
        ~SearchTreeMap();

        template <typename K>
        void put(key<K> key, T value);

        template <typename K>
        void put(K key, T value);

        template <typename K>
        void remove(key<K> key);

        template <typename K>
        void remove(K key);

        template <typename K>
        T operator [](key<K> key);

        template <typename K>
        T operator [](K key);

        size_t size();
        void clear();
    };

    template<typename T>
    template<typename K>
    T SearchTreeMap<T>::operator [](key<K> key) {
        lock.lock_read();
        auto node = traverse(key);
        T result;
        if (!node || !node->has_value) {
            result = sugar::zero_value_of<T>();
        } else {
            result = node->v;
        }
        lock.unlock_read();
        return result;
    }

    template<typename T>
    template<typename K>
    void SearchTreeMap<T>::put(key<K> key, T value) {
        auto cur_children = children;
        node<T>* cur_node = nullptr;
        bool created = true;

        lock.lock_write();
        for (size_t i = 0; i < key.size; i++) {
            auto parent = cur_node;
            void* k = key.deref ? reinterpret_cast<void*>(key.key) : (void*) &key.key;
            uint8_t src_k = ((uint8_t*)k)[i];

            cur_node = nullptr;
            for (node<T>* child : *cur_children) {
                if (child->k == src_k) {
                    cur_node = child;
                }
            }

            if (!cur_node) {
                cur_node = new node<T>{
                        .children = new_node_list<T>(),
                        .parent = parent,
                        .k = src_k,
                        .v = i == key.size - 1 ? value : sugar::zero_value_of<T>() ,
                        .has_value = i == key.size - 1,
                };
                cur_children->push_back(cur_node);
            } else if (i == key.size - 1) {
                cur_node->v = value;
                cur_node->has_value = true;
                created = false;
            }
            cur_children = cur_node->children;
        }
        if (created) {
            value_count++;
        }
        lock.unlock_write();
    }


    template<typename T>
    void SearchTreeMap<T>::remove_node(node<T>* node_to_remove) {

        auto parent = node_to_remove->parent;
        if (node_to_remove->parent) {
            NODE_LIST_REMOVE(*node_to_remove->parent->children, node_to_remove);
        } else {
            NODE_LIST_REMOVE(*children, node_to_remove);
        }
        delete node_to_remove->children;
        delete node_to_remove;

        auto parent_children = parent ? parent->children : children;
        while (parent) {
            if (!parent->has_value && parent_children->empty()) {
                auto next_parent = parent->parent;
                auto next_parent_children = children;
                if (next_parent) {
                    next_parent_children = next_parent->children;
                }
                NODE_LIST_REMOVE(*next_parent_children, parent);
                delete parent->children;
                delete parent;
                parent = next_parent;
                parent_children = next_parent_children;
            } else {
                break;
            }
        }

        value_count--;

    }

    template<typename T>
    template<typename K>
    void SearchTreeMap<T>::remove(key<K> key) {
        lock.lock_write();
        auto node = traverse(key);
        if (node && node->has_value) {
            remove_node(node);
        }
        lock.unlock_write();
    }

    template<typename T>
    template<typename K>
    node<T>* SearchTreeMap<T>::traverse(key<K> key) {
        auto cur_children = children;
        node<T>* cur_node = nullptr;

        for (size_t i = 0; i < key.size; i++) {
            cur_node = nullptr;
            void* k = key.deref ? reinterpret_cast<void*>(key.key) : (void*) &key.key;
            uint8_t src_k = ((uint8_t*)k)[i];
            for (node<T>* child : *cur_children) {
                if (child->k == src_k) {
                    cur_children = child->children;
                    cur_node = child;
                    break;
                }
            }
            if (cur_node == nullptr) {
                break;
            }
        }

        return cur_node;
    }

    template<typename T>
    template<typename K>
    inline T SearchTreeMap<T>::operator [](K key) {
        return (*this)[as_key(key)];
    }

    template<typename T>
    template<typename K>
    inline void SearchTreeMap<T>::put(K key, T value) {
        put(as_key(key), value);
    }

    template<typename T>
    template<typename K>
    void SearchTreeMap<T>::remove(K key) {
        remove(as_key(key));
    }

    template<typename T>
    SearchTreeMap<T>::~SearchTreeMap() {
        LOGD("SearchTreeMap destruct");
        clear();
        delete children;
    }

    template<typename T>
    size_t SearchTreeMap<T>::size() {
        lock.lock_read();
#ifdef SUPER_VERBOSE
        LOGDF("SearchTreeMap %p size: %zu\n", this, value_count);
#endif
        auto count = value_count;
        lock.unlock_read();
        return count;
    }

    template<typename T>
    void SearchTreeMap<T>::clear() {
        LOGD_STDERR("Clearing");
        auto cur_children = children;
        node<T>* cur_children_owner = nullptr;

        size_t depth = 0;

        lock.lock_write();

        while (!children->empty()) {
            CLEAR_LINE_STDERR();
            LOGDF_STDERR("Clearing\tdepth %zu\tcount %zu", depth, value_count);
            do {
                CLEAR_LINE_STDERR();
                LOGDF_STDERR("Clearing\tdepth %zu\tcount %zu\tchildren size %zu",
                             depth, value_count, cur_children->size());
                node<T>* child;
                if (cur_children->empty()) {
                    child = cur_children_owner;
                    if (!child || !child->parent) {
                        depth = 0;
                        break;
                    }
                    cur_children = child->parent->children;
                    cur_children_owner = child->parent;
                } else {
                    child = cur_children->back();
                }
                if (!child->children->empty()) {
                    cur_children = child->children;
                    depth++;
                } else {
                    auto next_children = children;
                    node<T>* next_children_owner = nullptr;
                    if (child->parent && child->parent->parent) {
                        next_children = child->parent->parent->children;
                        next_children_owner = child->parent->parent;
                    }
                    if (child->has_value) {
                        value_count--;
                    }
                    cur_children->pop_back();
                    delete child->children;
                    free(child);
                    if (cur_children->empty() && depth > 0) {
                        depth--;
                        cur_children = next_children;
                        cur_children_owner = next_children_owner;
                    }
                }
            } while (depth > 0);
        }

        LOGD_STDERR("\n");
        if (value_count != 0) {
            LOGWF("implementation is bad, value count is %zu (should be 0) after clearing. potential memory leak!",
                  value_count);
        }

        lock.unlock_write();
    }

}