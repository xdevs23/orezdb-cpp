#pragma once

#include "../threading/Channel.h"

using orezdb::threading::Channel;

#pragma push_macro("LOGD_S")
#pragma push_macro("LOGDF_S")
#ifdef DEBUG_QUEUES
#define LOGD_S(msg) LOGD("[SynchronizedQueue] " msg)
#define LOGDF_S(msg, fmt...) LOGDF("[SynchronizedQueue] " msg, fmt)
#else
#define LOGD_S(msg)
#define LOGDF_S(msg, fmt...)
#endif

namespace orezdb::collections {

    template <typename T>
    class SynchronizedQueue {
    private:
        Channel<T> chan{};
    public:
        void provide(T t);
        T consume();
    };

    template<typename T>
    void SynchronizedQueue<T>::provide(T t) {
        LOGD_S("provide(t)");
        chan.tx(t);
        LOGD_S("provide(t) -> done");
    }

    template<typename T>
    T SynchronizedQueue<T>::consume() {
        LOGDF_S("consume() q.size: %d\n", q.size());
        return chan.rx();
    }

}

#pragma pop_macro("LOGD_S")
#pragma pop_macro("LOGDF_S")