#pragma once

#ifdef DEBUG

#warning "Debug is enabled"
#define LOGD(msg) printf("D\t" msg "\n")
#define LOGDF(msg, fmt...) printf("D\t" msg, fmt)
#define LOGD_STDERR(msg) fprintf(stderr, msg)
#define LOGDF_STDERR(msg, fmt...) fprintf(stderr, msg, fmt)

#else
#define LOGD(msg)
#define LOGDF(msg, fmt...)
#define LOGD_STDERR(msg)
#define LOGDF_STDERR(msg, fmt...)
#endif

#define LOG LOGI
#define LOGF LOGIF

#define LOGW(msg) printf("W\t" msg "\n")
#define LOGWF(msg, fmt...) printf("W\t" msg, fmt)

#define LOGE(msg) printf("E\t" msg "\n")
#define LOGEF(msg, fmt...) printf("E\t" msg "\n", fmt)

#define LOGI(msg) printf("I\t" msg "\n")
#define LOGIF(msg, fmt...) printf("I\t" msg "\n", fmt)

#define LOG_FLUSH() fflush(stdout);
#define CLEAR_LINE() printf("\033[K\r")
#define CLEAR_LINE_STDERR() fprintf(stderr, "\033[K\r")

#define LOG_IF(cond, msg) { \
    if (cond) {             \
        LOG(msg);           \
    }                       \
}

