#pragma once

#include <mutex>

#include "Waiter.h"

using std::mutex;

namespace orezdb::threading {

    template<typename T>
    class Future {
    private:
        Waiter waiter{};
        T value;

    public:
        T get();
        void set(T value);
    };

    template<typename T>
    T Future<T>::get() {
        waiter.wait();
        return value;
    }

    template<typename T>
    void Future<T>::set(T val) {
        value = val;
        waiter.notify();
    }

}