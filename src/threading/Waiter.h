#pragma once

#include <condition_variable>
#include <mutex>
#include <atomic>
#include "ReadWriteLock.h"
#include "Lock.h"
#include "Channel.h"

using std::condition_variable;
using std::mutex;
using std::unique_lock;
using std::atomic;

namespace orezdb::threading {

    class Waiter {
    private:
        Channel<void*> chan{};
        atomic<bool> has_notified = false;
        bool latched = false;

    public:
        Waiter();
        void notify();
        void wait();
        void reset();
    };

}
