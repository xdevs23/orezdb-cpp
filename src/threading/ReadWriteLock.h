#pragma once

#include <shared_mutex>

#include "../log.h"

#ifdef DEBUG_LOCKS
#define LOG_(msg) LOGD(msg)
#define LOGF_(msg, fmt...) LOGDF(msg, fmt)
#else
#define LOG_(msg)
#define LOGF_(msg, fmt...)
#endif

namespace orezdb::threading {

    class ReadWriteLock {
    private:
        std::shared_mutex mutex{};

    public:
        const char* name;

        explicit ReadWriteLock(const char* lockName = "");

#ifndef DEBUG_LOCKS
        inline
#endif
        void lock_read() {
            LOGF_("[ReadWriteLock %p %s] lock_read request\n", this, name);
            mutex.lock_shared();
            LOGF_("[ReadWriteLock %p %s] lock_read success\n", this, name);
        }

#ifndef DEBUG_LOCKS
        inline
#endif
        void unlock_read() {
            LOGF_("[ReadWriteLock %p %s] unlock_read\n", this, name);
            mutex.unlock_shared();
        }

#ifndef DEBUG_LOCKS
        inline
#endif
        void lock_write() {
            LOGF_("[ReadWriteLock %p %s] lock_write request\n", this, name);
            mutex.lock();
            LOGF_("[ReadWriteLock %p %s] lock_write success\n", this, name);
        }

#ifndef DEBUG_LOCKS
        inline
#endif
        void unlock_write() {
            LOGF_("[ReadWriteLock %p %s] unlock_write\n", this, name);
            mutex.unlock();
        }
    };

}