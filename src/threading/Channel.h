#pragma once

#include <list>
#include <mutex>
#include <condition_variable>

using std::list;
using std::mutex;
using std::condition_variable;
using std::unique_lock;

namespace orezdb::threading {

    template<typename T>
    class Channel {
    private:
        list<T> q{};
        mutex mut{};
        condition_variable cond{};

    public:
        void tx(T t);
        T rx();
        void reset();
    };

    template<typename T>
    T Channel<T>::rx() {
        unique_lock<mutex> lock(mut);
        cond.wait(lock, [this]() {
            return !q.empty();
        });
        T result = q.front();
        q.pop_front();
        return result;
    }

    template<typename T>
    void Channel<T>::tx(T t) {
        unique_lock<mutex> lock(mut);
        q.push_back(t);
        cond.notify_all();
    }

    template<typename T>
    void Channel<T>::reset() {
        mut.lock();
        q.clear();
        mut.unlock();
    }

}