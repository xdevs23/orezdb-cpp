#pragma once

namespace orezdb::threading {
    void join_thread(thread& t);
    void set_thread_name(thread& t, const char* name);
}