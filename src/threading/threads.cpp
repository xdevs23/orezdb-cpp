#include <thread>
#include <stdexcept>
#ifdef __linux__
#include <pthread.h>
#endif

using std::thread;

namespace orezdb::threading {

    void join_thread(thread& t) {
        if (t.joinable()) {
            try {
                t.join();
            } catch (std::invalid_argument&) {
                // It already exited.
            }
        }
    }

    void set_thread_name(thread& t, const char* name) {
#ifdef __linux__
        pthread_t native_t = t.native_handle();
        pthread_setname_np(native_t, name);
#endif
    }
}