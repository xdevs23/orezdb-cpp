#include <pthread.h>

#include "Lock.h"

namespace orezdb::threading {

    Lock::Lock() {
        pthread_mutex_init(&mutex, nullptr);
    }

    void Lock::lock() {
        pthread_mutex_lock(&mutex);
    }

    void Lock::unlock() {
        pthread_mutex_unlock(&mutex);
    }

}

