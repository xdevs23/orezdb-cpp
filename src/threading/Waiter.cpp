
#include <stdexcept>

#include "Waiter.h"
#include "Channel.h"
#include "../log.h"

using std::runtime_error;

namespace orezdb::threading {

    Waiter::Waiter() {
        reset();
    }

    void Waiter::notify() {
        chan.tx(nullptr);
    }

    void Waiter::wait() {
        chan.rx();
    }

    void Waiter::reset() {
        chan.reset();
    }
}
