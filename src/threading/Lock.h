#pragma once

#include <atomic>

using std::atomic;

namespace orezdb::threading {

    class Lock {
        pthread_mutex_t mutex;
    public:
        Lock();
        void lock();
        void unlock();
    };
}