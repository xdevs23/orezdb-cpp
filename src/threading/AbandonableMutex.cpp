
#include <stdexcept>

#include "AbandonableMutex.h"
#include "../log.h"

using std::runtime_error;

namespace orezdb::threading {

    void AbandonableMutex::lock() {
        abandon_mutex.lock();
        if (isAbandoned) {
            LOGW("Mutex is already abandoned, cannot lock!");
            abandon_mutex.unlock();
            throw runtime_error("Already abandoned mutex cannot be locked");
        }
        mut.lock();
        abandon_mutex.unlock();
    }

    void AbandonableMutex::unlock() {
        mut.unlock();
    }

    void AbandonableMutex::abandon() {
        abandon_mutex.lock();
        isAbandoned = true;
        abandon_mutex.unlock();
    }
}
