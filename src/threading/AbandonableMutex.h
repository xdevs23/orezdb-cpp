#pragma once

#include <mutex>

using std::mutex;

namespace orezdb::threading {

    class AbandonableMutex {
    private:
        bool isAbandoned;
        mutex mut{};
        mutex abandon_mutex{};

    public:
        void lock();
        void unlock();
        void abandon();
    };

}
