#!/usr/bin/env bash

set -euo pipefail

case $1 in
  sources)
    find src/ -type f -name "*.c" -o -name "*.cpp"
    ;;
  headers)
    find src/ -type f -name "*.h" -o -name "*.hpp"
    ;;
  matchingheader)
    srcfile="$2"
    if [ -z "$srcfile" ]; then
      echo "No source file specified" >&2
      exit 1
    fi

    headerfile="$(echo "$srcfile" | sed -re 's/.c(pp)?$/.h/g')"
    if [ -f "$headerfile" ]; then
      echo "$headerfile"
    elif [[ "$srcfile" == *".cpp" ]]; then
      headerfile="$(echo "$srcfile" | sed -re 's/.cpp?$/.hpp/g')"
      if [ -f "$headerfile" ]; then
        echo "$headerfile"
      fi
    fi
    ;;
esac
