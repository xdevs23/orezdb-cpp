#!/usr/bin/env bash

cat <<\EOF > Makefile
$(shell mkdir -p bin obj)

CFLAGS ?=
LDFALGS ?=
DEBUG ?=
CC := clang
LD := $(CC)

LIBS := pthread stdc++
LIBS_LD_FLAGS := $(foreach f, $(LIBS), -l$f)
LDFLAGS += $(LIBS_LD_FLAGS)

ALWAYS_CAUSE_RECOMPILE := \
  src/log.h Makefile

EOF

cat <<EOF >> Makefile

HEADERS := $(./find.sh headers | tr "\n" " ")
ALWAYS_CAUSE_RECOMPILE += \$(HEADERS)

.PHONY: all
all: bin/main

EOF

if [ -n "$CFLAGS" ]; then
  echo "Additional CFLAGS: $CFLAGS"
  cat <<EOF >> Makefile

CFLAGS += $CFLAGS

EOF
fi

if [ -n "$LDFLAGS" ]; then
  echo "Additional LDFLAGS: LDFLAGS"
  cat <<EOF >> Makefile

LDFLAGS += $LDFLAGS

EOF
fi

DEBUG=$(if [ "$DEBUG" == "true" ]; then echo "true"; else echo "false"; fi)
HEAP_DEBUG=$(if [ "$HEAP_DEBUG" == "true" ]; then echo "true"; else echo "false"; fi)
echo "Debug: $DEBUG"

if $DEBUG; then
  cat <<\EOF >> Makefile

CFLAGS += -DDEBUG

EOF
fi

if $HEAP_DEBUG; then
cat <<\EOF >> Makefile

CFLAGS += -DHEAP_DEBUG

EOF
fi

if $DEBUG && [ "$PERFORMANCE" != "true" ]; then
  cat <<\EOF >> Makefile

CFLAGS += -g -O0
LDFLAGS += -g -O0

EOF
else
  cat <<\EOF >> Makefile

CFLAGS += -g0 -O3
LDFLAGS += -g0 -O1

EOF
fi

while read -r srcfile; do
  objfile="obj/$(echo "$srcfile" | sed -re 's/[.]c(pp)?$/.o/g' -re 's/^src\/(.*)$/\1/g')"
  echo "$srcfile -> $objfile" >&2
  # TODO: uhhh yea incremental compilation currently happens less often than it should... fix that
  cat <<EOF >> Makefile
$objfile: $srcfile \$(ALWAYS_CAUSE_RECOMPILE) $(./find.sh matchingheader "$srcfile")
	mkdir -p \$(shell dirname $objfile)
	\$(CC) \$(CFLAGS) -std=c++17 -c \$< -o \$@
EOF
done < <(./find.sh sources)

cat <<EOF >> Makefile
bin/main: $(./gen.sh objpaths | tr "\n" " ")
	\$(LD) \$(LDFLAGS) -o \$@ \$^

.PHONY: clean
clean:
	rm -rf bin obj
EOF

